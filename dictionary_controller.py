# netid: vcox2, ymohamed

import random
import cherrypy
import re, json

class DictionaryController:
    def __init__(self):
        self.myd = dict()

    # This class is used to hold event handlers
    def GET_KEY(self, key):
        output = {'result':'success'}
        key = str(key)
        try:
            val = self.myd[key]
            output['key'] = key
            output['value'] = val
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)
        return json.dumps(output)

    def GET_INDEX(self):
        output = {'result':'success'}
        output['entries'] = list()
        for num,key in enumerate(self.myd.keys()):
            try:
                val = self.myd[key]
                temp_dict = dict()
                temp_dict['key'] = key
                temp_dict['value'] = val
                output['entries'].append(temp_dict)
            except KeyError as ex:
                output['result'] = 'error'
                output['message'] = 'key not found'
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)
        return json.dumps(output)
    
    def DELETE_KEY(self, key):
        output = {'result':'success'}
        key = str(key)
        try:
            val = self.myd[key]
            self.myd.pop(key)
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        return json.dumps(output)

      
    def DELETE_INDEX(self):
        output = {'result':'success'}
        try:
            self.myd = dict()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)   
            print(self.myd)       
            print(str(ex))
        return json.dumps(output)
        
      
    def POST_INDEX(self):
        output = {'result':'success'}
        data = cherrypy.request.body.read()
        data_json = json.loads(data)
        key = data_json['key']
        val = data_json['value']
        self.myd[key] = val
        return json.dumps(output)
    	
    
    def PUT_KEY(self, key):
        output = {'result':'success'}
        data = cherrypy.request.body.read()
        data_json = json.loads(data)
        key = str(key)
        val = data_json['value']
        self.myd[key] = val
        return json.dumps(output)
